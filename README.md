# NAME

Alien::PGPLOT - Build and Install the PGPLOT library

# VERSION

version v5.2.2.7

# SYNOPSIS

    use Alien::PGPLOT;

# DESCRIPTION

This module finds or builds the _PGPLOT_ library.

# USAGE

Please see [Alien::Build::Manual::AlienUser](https://metacpan.org/pod/Alien%3A%3ABuild%3A%3AManual%3A%3AAlienUser) (or equivalently on [metacpan](https://metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manual/AlienUser.pod)).

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-alien-pgplot@rt.cpan.org  or through the web interface at: https://rt.cpan.org/Public/Dist/Display.html?Name=Alien-PGPLOT

## Source

Source is available at

    https://gitlab.com/djerius/alien-pgplot

and may be cloned from

    https://gitlab.com/djerius/alien-pgplot.git

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2021 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
