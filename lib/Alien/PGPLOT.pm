package Alien::PGPLOT;

# ABSTRACT: Build and Install the PGPLOT library

use strict;
use warnings;

our $VERSION = 'v5.2.2.7';

use base qw( Alien::Base );

1;

# COPYRIGHT

__END__

=pod

=for stopwords
metacpan


=head1 SYNOPSIS

  use Alien::PGPLOT;

=head1 DESCRIPTION

This module finds or builds the I<PGPLOT> library.

=head1 USAGE

Please see L<Alien::Build::Manual::AlienUser> (or equivalently on L<metacpan|https://metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manual/AlienUser.pod>).

